package com.arun.aashu.bottomnavdrawer;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.arun.aashu.bottomnavdrawer.adapters.ItemArrayAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener {

   private ActionBarDrawerToggle toggle;
   private   DrawerLayout drawerLayout;

    private  TextView tvMsg;
    ArrayList<Pojo>  arrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvMsg = (TextView) findViewById(R.id.tv_msg);



         drawerLayout=findViewById(R.id.drawer_layout);

        final RecyclerView recyclerViewNumbers = (RecyclerView) findViewById(R.id.list);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewNumbers.setLayoutManager(linearLayoutManager);



         toggle=new ActionBarDrawerToggle(MainActivity.this,
                drawerLayout,null,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        NavigationView navigationView=findViewById(R.id.navigationView1);
//        navigationView.setNavigationItemSelectedListener(this);


        String data[]=getResources().getStringArray(R.array.numbers);




        final ArrayList mylist  = new ArrayList<String>(Arrays.asList(data));


        ItemArrayAdapter itemArrayAdapter=new ItemArrayAdapter(MainActivity.this, data);

        recyclerViewNumbers.setAdapter(itemArrayAdapter);

        recyclerViewNumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(MainActivity.this, "position :-"+mylist, Toast.LENGTH_SHORT).show();


            }
        });

}


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item))
        {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {



        switch (item.getItemId()){

            case R.id.nav_Home :
                Toast.makeText(this, "home", Toast.LENGTH_SHORT).show();
                break;


        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (toggle != null)
            toggle.syncState();
    }



}
