package com.arun.aashu.bottomnavdrawer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.arun.aashu.bottomnavdrawer.Pojo;
import com.arun.aashu.bottomnavdrawer.R;

import java.util.ArrayList;

/**
 * Created by aAsHu on 4/27/2018.
 */

public class ItemArrayAdapter extends RecyclerView.Adapter<ItemArrayAdapter.MyViewHolder> {

    ArrayList<Pojo> pojoList;
    ArrayList<String> stringList;
    String[] stringArray;

    Context context;
    LayoutInflater layoutInflater;
    View view;

    public ItemArrayAdapter() {
    }

    public ItemArrayAdapter(ArrayList<Pojo> pojoList, Context context) {
        this.pojoList = pojoList;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

    }

    public ItemArrayAdapter(Context context, ArrayList<String> stringList) {
        this.stringList = stringList;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

    }

    public ItemArrayAdapter(Context context, String[] stringArray) {
        this.stringArray = stringArray;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = layoutInflater.inflate(R.layout.list_item, null, false);

        ItemArrayAdapter.MyViewHolder myViewHolder = new ItemArrayAdapter.MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        holder.textView.setText(stringArray[position]);


    }

    @Override
    public int getItemCount() {
        return stringArray.length;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public MyViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_text);

        }
    }
}
